-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here
display.setStatusBar( display.HiddenStatusBar )
local chart = require "coronachartslib.chart"

local g = display.newGroup()


local data = {
  radius = 70,
  values = {
    {percentage = 30, color = {0,0,255}},
    {percentage = 50, color = {0,255,0}},
    {percentage = 8, color = {255,0,0}},
    {percentage = 7, color = {0,255,255}},
    {percentage = 5, color = {255,0,255}},
  },
  name = "myFirstPieChart.png"
};

local pie = chart.displayPie(data);
pie.x=-150 pie.y=-100
g:insert(pie)



local barChart = chart.displayBarChart(data)
barChart.x = 350; barChart.y=-80
g:insert(barChart)



local plotData = {
	width = 280,
	height = 130,
	maxlenght = 10,
	values = {
		{points={0,1,3,10,7,7,10,3,1,0}, color = {0,0,255}},
		{points={5,2,4,8,6,9,7}, color = {0,255,255}},
	}
}


local plot = chart.displayplot(plotData)
plot.x=140 plot.y=180




